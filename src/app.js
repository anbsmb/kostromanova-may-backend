﻿const express = require("express");
const {Pool} = require('pg');
import {getFromConfig, handleDefault, db} from './utils';

const hostname = process.env.IP_ADDRESS || '0.0.0.0';
const port = 8000;
const app = express();
const savePool = (req, res, next) => {
    req.pool = pool;
    next();
};

var pool = null;
const connectToDataBase = () => {
    pool = new Pool(getFromConfig('postgresql'));
}

const ALLOWED_ORIGINS = [
    'http://168.63.58.52:80',
    'http://localhost:63342',
    'http://localhost:80',
    'http://ksutechrosset.northeurope.cloudapp.azure.com'
];

const testServer = () => {
    // DB
    console.log(db.queries.select('users', { login: 'Петя', role: 1 }));
    console.log(db.queries.insert('users', { login: 'Петя', ddd: 1 }));
    console.log(db.queries.update('users', { login: 'Петя', ddd: 1 }, { user_id: 1 }));
    console.log(db.queries.delete('users', { login: 'Петя', ddd: 1 }));

    console.log(db.queries.user.setDocuments({ user_id: 1 }));
    console.log(db.queries.project.setDocuments({ project_id: 1 }));
}

app.use(function (req, res, next) {
    if(ALLOWED_ORIGINS.indexOf(req.headers.origin) > -1) {
        res.set('Access-Control-Allow-Credentials', 'true')
        res.set('Access-Control-Allow-Origin', req.headers.origin)
    } else { // разрешить другим источникам отправлять неподтвержденные запросы CORS
        res.set('Access-Control-Allow-Origin', '*')
    }

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, x-ijt, Authorization');


    next();
});

app.use(express.json({ extended: true }));
app.use(savePool);
app.use('/api/user/', require('./routes/user.routes'));
app.use('/api/tracks/', require('./routes/tracks.routes'));
app.use('/api/lines/', require('./routes/lines.routes'));
app.use('/api/tasks/', require('./routes/tasks.routes'));
app.use('/api/cars/', require('./routes/cars.routes'));

app.listen(port, hostname, async () => {
    try {
        await connectToDataBase();
        console.log(`Server running at http://${hostname}:${port}/`);
        // testServer();
    } catch(error) {
        handleDefault(error);
    }
});