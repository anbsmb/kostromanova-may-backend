const {Router} = require('express');
import {db, wrapResponse, handleDefault} from '../utils';
const router = Router();

// /api/cars/getAll
router.get(
    '/getAll',
    wrapResponse((request, response) => {
        let cars = null
        request.pool.connect()
            .then(client => {
                client.query(db.queries.select('Car', null, 'ct."Velocity"',
                    'LEFT JOIN "Car_type" ct ON ct."id_type" = t."Car_type"')
                )
                    .then(db.getAll)
                    .then((result) => {
                       cars = result;
                       const carIds = result.map((car) => car.id_car);

                       return client.query(db.queries.select('Task_cars',null,
                           'ts.*',
                           'LEFT JOIN "Task" ts ON ts."id_task" = t."Task"'
                           , `WHERE t."Car" IN (${carIds.join(', ')})`));
                    })
                    .then(db.getAll)
                    .then((result) => {
                        const res = cars.map((car) => ({
                            ...car,
                            car_tasks: result.filter((task_car) => task_car.Car === car.id_car)
                                             .map((task_car) => ({
                                                 Task: task_car.Task,
                                                 From: task_car.From,
                                                 To: task_car.To
                                             }))
                        }));

                        client.release();
                        response.json(res);
                    })
                    .catch((e) => {
                        client.release();
                        handleDefault(e, response);
                    });
            });
    })
);

module.exports = router;