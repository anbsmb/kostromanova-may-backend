const {Router} = require('express');
import {db, wrapResponse, handleDefault} from '../utils';
const router = Router();

// /api/tracks/getAll
router.get(
    '/getAll',
    (request, response) => {
        request.pool
            .query(db.queries.select('Track'))
            .then(db.getAll)
            .then((result) => response.json(result))
            .catch((e) => handleDefault(e, response));
    }
);

// /api/tracks/task/update
router.post(
    '/task/update',
    wrapResponse((request, response) => {
        const track_tasks = request.body;
        const queriesAmount = track_tasks.length;

        request.pool.connect()
            .then(client => {
                const getQueryByTable = (data, table) => {
                    if (table === 'Task_tracks') {
                        db.queries.update(table, {
                            From: data.from,
                            To: data.to
                        }, {
                            Track: data.track,
                            Task: data.task
                        });
                    } else if (table === 'Task_cars') {
                        db.queries.insert(table, {
                            Car: data.carId,
                            Task: data.task
                        });
                    }
                };

                const runAsync = (index, table) => {
                    if (index >= queriesAmount) {
                        return Promise.resolve(true);
                    }

                    const newQuery = track_tasks[index];
                    return client.query(getQueryByTable(newQuery, table))
                        .then(db.getOne)
                        .then((result) =>  {
                            if (!result) {
                                handleDefault(new Error('Не удалось изменить данные трэк-таску'), response);
                            } else {
                                return runAsync(++index, table);
                            }
                        });
                };

                runAsync(0, 'Task_tracks')
                    .then(() => runAsync(0, 'Task_cars'))
                    .then((result) => {
                        client.release();
                        response.json(result);
                    }).catch((error) => {
                    client.release();
                    handleDefault(error, response);
                })
            });
    })
);

module.exports = router;