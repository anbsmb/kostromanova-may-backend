const {Router} = require('express');
import {db, handleDefault} from '../utils';
const router = Router();

// /api/lines/getAll
router.get(
    '/getAll',
    (request, response) => {
        request.pool
            .query(db.queries.select('Line'))
            .then(db.getAll)
            .then((result) => response.json(result))
            .catch((e) => handleDefault(e, response));
    }
);

module.exports = router;