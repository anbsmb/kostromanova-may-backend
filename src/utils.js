const sql = require('yesql').pg;
var config = require('config');

export const getFromConfig = (query) => {
    if (config.has(query)) {
        return config.get(query);
    }

    throw new Error(`Error getting "${query}" from config`);
}

// WRAPPERS
export const wrapSql = (queryString, data) => sql(queryString)(data);
export const handleDefault = (error, response) => {
    if (error !== 'internal') {
        console.log(error);
        if (response) {
            response.status(500).json({ message: `Ошибка на сервере '${error.message}'`, error });
        }
    }
};
export const wrapResponse = (func) => {
    return (request, response, next) => {
        try {
            func(request, response, next);
        } catch (error) {
            return handleDefault(error, response);
        }
    }
};
export const wrapAccess = (func, accessArray) => {
    return (request, response, next) => {
        func(request, response, next, accessArray);
    };
};
export const getWhere = (data, tableAlias) =>
    data && typeof data === 'object' && Object.keys(data).length
        ? `WHERE ${Object.keys(data).map((key) => `${tableAlias ? `${tableAlias}.` : ''}${key} = :${key}`).join(' AND ')}`
        : '';

// DB HELPERS
export const db = {
    getOne: (result) => result.rows?.[0],
    getAll: (result) => result.rows,
    queries: {
        // SELECT
        select: (table, data, additionalSelect, joins, endQuery, needWhere) => {
            const queryString = `SELECT t.* ${additionalSelect ? ', ' : ''}${additionalSelect || ''}
                              FROM public."${table}" as t ${joins || ''} ${needWhere === false ? '' : getWhere(data, 't')} ${endQuery || ''}`;
            // console.log(queryString);
            return wrapSql(queryString, data);
        },
        // INSERT
        insert: (table, data) => {
            const queryString = `INSERT INTO public."${table}" as t (${Object.keys(data).map((key) => `"${key}"`).join(', ')}}) VALUES (${
                Object.keys(data).map((key) => `:${key}`).join(', ')
            }) RETURNING *`;

            return wrapSql(queryString, data);
        },
        // UPDATE
        update: (table, data, whereData) => {
            const queryString = `UPDATE public."${table}" SET ${
                Object.keys(data).map((key) => `${key} = :${key}`).join(', ')
            } ${getWhere(whereData)} RETURNING *`;
            const wrapData = {...data, ...whereData};
            return wrapSql(queryString, wrapData);
        },
        // DELETE
        delete: (table, data) => {
            const queryString = `DELETE FROM public."${table}" ${getWhere(data, 't')}`;
            return wrapSql(queryString, data);
        },

        // API
        user: {
            getAllUsers: () => sql(`
            SELECT   user_id, login, role, password, firstname,
                     lastname, surname, company,
                     department, "position", r.region_name
            FROM users as u
            LEFT JOIN regions as r on r.region_id = u.region_id
         `)({})
        }
    }
};