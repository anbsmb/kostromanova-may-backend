FROM node:16-alpine3.11
WORKDIR /app
COPY . .
EXPOSE 8000
CMD [ "node", "dist/app.js" ]
